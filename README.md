# Dotfiles
Arch Linux user configuration, mainly for my own convenience. There are two branches, each for a different machine, with very minor differences between them (e.g., font size, monitor order, etc.).

The current setup looks as follows:

## System
- Bootloader:               Systemd-boot
- Initramfs generator:      Booster
- Kernel:                   Linux Zen
- Shell:                    Zsh (no good reason, might go back to bash)

## Desktop
- App menu:                 Rofi-wayland
- Compositor:               Hyprland
- Display manager:          SDDM (until ly works again :))
- Display server protocol:  Wayland
- Lock screen method:       Hypridle + Hyprlock
- Power menu:               Rofi-waylad
- Status bar:               Waybar
- Wallpaper setter:         Hyprpaper with (custom) Hyprpaper-gen

## Preferred Apps
- Document viewer:          Zathura, Foliate, and occasionally Sioyek
- Dotfiles manager:         GNU Stow with git
- Editor:                   Neovim. Now using code (open source vscode) with neovim plugin for Python
- Email client:             Thunderbird
- File manager:             Yazi (nightly)/ Thunar
- File sync:                Syncthing with a 4 device setup / Nextcloud
- Image viewer:             Feh
- Password manager:         KeePassXC
- System monitor:           Btop
- Terminal:                 Kitty
- Web browser:              Firefox

## Other settings
- Colorscheme:              Gruvbox whenever possible
- Remap caps lock:          Keyd
