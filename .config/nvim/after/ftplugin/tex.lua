-- tex specific configuration

-- this is being overwritten by the guess-indent plugin
vim.opt.tabstop = 4
vim.opt.shiftwidth = 4
