local line_begin = require("luasnip.extras.expand_conditions").line_begin

return {
  s({trig="nn", snippetType="autosnippet"},
    fmta(
      [[
      ```{<>}
      <>
      ```
      ]],
      {
        i(1),
        i(2),
      }
    ),
    {condition = line_begin}
  ),
  -- in-line math
  s({trig = "([^%a])mm", wordTrig = false, regTrig = true, snippetType="autosnippet"},
    fmta(
      "<>$<>$",
      {
        f( function(_, snip) return snip.captures[1] end ),
        d(1, get_visual),
      }
    )
  ),

}
