return {

  'nvim-tree/nvim-tree.lua',          -- file tree

  dependencies = {
    'nvim-tree/nvim-web-devicons',      -- icons for the file tree
  },

  config = function ()
    vim.g.loaded_netrw = 1
    vim.g.loaded_netrwPlugin = 1

    require("nvim-tree").setup()

    vim.keymap.set('n', '<c-n>', ':NvimTreeFindFileToggle<CR>')


    -- remove eob ~: https://github.com/nvim-tree/nvim-tree.lua/discussions/1582
    local Api = require('nvim-tree.api')
    local Event = require('nvim-tree.api').events.Event

    Api.events.subscribe(Event.TreeOpen, function()
      vim.opt_local.fillchars="eob: "
    end)
  end
}
