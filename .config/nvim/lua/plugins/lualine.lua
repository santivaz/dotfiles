return {
  'nvim-lualine/lualine.nvim',        -- line at the bottom

  config = function ()
    require('lualine').setup {
      options = {
        incons_enabled = true,
        theme = 'gruvbox',
      },
      sections = {
        lualine_a = {
          {
            'filename',
            path = 1,
          }
        }
      }
    }
  end
}
