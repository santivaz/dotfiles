vim.keymap.set('n', '<leader>ts', ':ASToggle<CR>', {desc = "toggle autosave"})

return {
  'Pocco81/auto-save.nvim',           -- autosave
}
