return {
  'nvim-telescope/telescope.nvim',
  dependencies = { 'nvim-lua/plenary.nvim' },
  config = function ()
    require('telescope').setup()
    local builtin = require('telescope.builtin')

    vim.keymap.set('n', '<leader>ff', builtin.find_files, {desc = "find file"})
    vim.keymap.set('n', '<leader>fg', builtin.live_grep, {desc = "grep"})
    vim.keymap.set('n', '<leader>fb', builtin.buffers, {desc = "find buffer"})
    vim.keymap.set('n', '<leader>fh', builtin.help_tags, {desc = "find help"})

  end
}
