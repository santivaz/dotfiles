return {

  'nvim-treesitter/nvim-treesitter',    -- syntax highlight and more
  dependencies = { -- some optional deps
    'luckasRanarison/tree-sitter-hyprlang', -- recognise hyprland config files
  },
  run = ':TSUpdate', -- why is this here? (quarto told me but why?)
  config = function ()
    require'nvim-treesitter.configs'.setup {
      -- A list of parser names, or "all"
      ensure_installed = { "python", "lua", "vim", "latex", "julia" },

      -- Install parsers synchronously (only applied to `ensure_installed`)
      sync_install = false,
      auto_install = true,
      highlight = {
        enable = true,
        disable = { "latex" },
      },
      indent = {
        enable = false,
      },
    }

    -- parser of Hyprland config files
    local parser_config = require("nvim-treesitter.parsers").get_parser_configs()
    parser_config.hypr = {
      install_info = {
        url = "https://github.com/luckasRanarison/tree-sitter-hypr",
        files = { "src/parser.c" },
        branch = "master",
      },
      filetype = "hypr",
    }
  end,
}
