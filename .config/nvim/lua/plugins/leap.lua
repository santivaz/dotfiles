return  {
  "ggandor/leap.nvim",
  lazy = false,
  config = function()
    -- require('leap').create_default_mappings()
    vim.keymap.set({'n', 'x', 'o'}, 's',  '<Plug>(leap-forward)', {desc = "Leap forward"})
    vim.keymap.set({'n', 'x', 'o'}, 'S',  '<Plug>(leap-backward)', {desc = "Leap backward"})
    vim.keymap.set({'n', 'x', 'o'}, 'gs', '<Plug>(leap-from-window)', {desc = "Leap from window"})
  end,
}
