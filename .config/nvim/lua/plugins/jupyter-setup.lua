return {
  -- https://github.com/benlubas/molten-nvim/blob/main/docs/Notebook-Setup.md
  {
    'GCBallesteros/jupytext.nvim',
    config = function ()
      require("jupytext").setup({
        style = "markdown",
        output_extension = "md",
        force_ft = "markdown",
        -- style = "quarto",
        -- output_extension = "qmd",
        -- force_ft = "quarto",
      })
    end
  },



  {
    "3rd/image.nvim",
    -- version = "1.1.0",
    dependencies = {
      'leafo/magick',
    },
    -- dependencies = {
    --   "vhyrro/luarocks.nvim",
    --   priority = 1001,
    --   opts = {
    --     rocks = { "magick" },
    --   },
    -- },
    opts = {
      backend = "kitty",
      -- integrations = {},
      integrations = {
        markdown = {
          enabled = true,
          clear_in_insert_mode = false,
          download_remote_images = true
        },
      },
      max_width = 200,
      max_height = 30,
      max_width_window_percentage = math.huge,
      max_height_window_percentage = math.huge,
      window_overlap_clear_ft_ignore = { "cmp_menu", "cmp_docs", "" },
    },
  },



  {
    'benlubas/molten-nvim',           -- run code interactively with the jupyter kernel
    ft = {"markdown", "quarto"},
    build = ":UpdateRemotePlugins",
    dependencies = { '3rd/image.nvim' },
    config = function ()
      -- minimum suggested

      -- https://github.com/benlubas/molten-nvim/blob/main/docs/Notebook-Setup.md

      -- I find auto open annoying, keep in mind setting this option will require setting
      -- a keybind for `:noautocmd MoltenEnterOutput` to open the output again
      vim.g.molten_auto_open_output = false
      -- I don't think this does anything
      vim.g.molten_enter_output_behaviour = "open_then_enter"

      vim.g.molten_wrap_output = false
      -- Output as virtual text. Allows outputs to always be shown.
      vim.g.molten_virt_text_output = true
      -- Images are supper buggy on virt text so it's disabled for now
      vim.g.molten_image_location = "float"
      -- vim.g.molten_auto_image_popup = true
      -- this will make it so the output shows up below the \`\`\` cell delimiter
      vim.g.molten_virt_lines_off_by_1 = true
      vim.g.molten_use_border_highlights = true

      vim.g.molten_image_provider = "image.nvim"


      vim.keymap.set("n", "<localleader>mi", ":MoltenInit<CR>", { silent = true, desc = "Initialize Molten" })

      vim.keymap.set("n", "<localleader>os", ":noautocmd MoltenEnterOutput<CR>", { desc = "open output window", silent = true })
      vim.keymap.set("n", "<localleader>oh", ":MoltenHideOutput<CR>", { desc = "close output window", silent = true })
      vim.keymap.set("n", "<localleader>oi", ":MoltenImagePopup<CR>", { desc = "open output image in popup", silent = true })

      -- vim.keymap.set("n", "<localleader>e", ":MoltenEvaluateOperator<CR>", { desc = "evaluate operator", silent = true })
      -- vim.keymap.set("n", "<localleader>rl", ":MoltenEvaluateLine<CR>", { silent = true, desc = "evaluate line" })
      -- vim.keymap.set("n", "<localleader>rr", ":MoltenReevaluateCell<CR>", { desc = "re-eval cell", silent = true })
      -- vim.keymap.set("v", "<localleader>r", ":<C-u>MoltenEvaluateVisual<CR>gv", { desc = "execute visual selection", silent = true })
      -- vim.keymap.set("n", "<localleader>md", ":MoltenDelete<CR>", { desc = "delete Molten cell", silent = true })
      -- vim.keymap.set("n", "<localleader>mx", ":MoltenOpenInBrowser<CR>", { desc = "open output in browser", silent = true })


    end
  },



  {
    'quarto-dev/quarto-nvim',
    ft = {"quarto", "markdown"},
    dependencies = {
      'jmbuhr/otter.nvim',
    },
    config = function ()
      require('quarto').setup{
        debug = false,
        closePreviewOnExit = true,
        lspFeatures = {
          enabled = true,
          chunks = "all",
          languages = { "r", "python", "julia", "bash", "html" },
          diagnostics = {
            enabled = true,
            triggers = { "BufWritePost" },
          },
          completion = {
            enabled = true,
          },
        },
        codeRunner = {
          enabled = true,
          default_method = "molten", -- 'molten' or 'slime'
          ft_runners = {}, -- filetype to runner, ie. `{ python = "molten" }`.
          -- Takes precedence over `default_method`
          never_run = { "yaml" }, -- filetypes which are never sent to a code runner
        },
      }
      vim.treesitter.language.register("markdown", { "quarto", "rmd" }) -- https://github.com/jmbuhr/otter.nvim/issues/179

      vim.keymap.set('n', '<localleader>qp', require('quarto').quartoPreview, { silent = true, noremap = true, desc = "open quarto preview" })

      local runner = require("quarto.runner")
      vim.keymap.set("n", "<localleader>rc", runner.run_cell,  { desc = "run cell", silent = true })
      vim.keymap.set("n", "<localleader>ra", runner.run_above, { desc = "run cell and above", silent = true })
      vim.keymap.set("n", "<localleader>rA", runner.run_all,   { desc = "run all cells", silent = true })
      vim.keymap.set("n", "<localleader>rl", runner.run_line,  { desc = "run line", silent = true })
      vim.keymap.set("v", "<localleader>r",  runner.run_range, { desc = "run visual range", silent = true })
      vim.keymap.set("n", "<localleader>RA", function()
        runner.run_all(true)
      end, { desc = "run all cells of all languages", silent = true })
    end
  },
}
