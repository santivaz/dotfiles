return {

"neovim/nvim-lspconfig",
dependencies = {
    "williamboman/mason.nvim",
    "williamboman/mason-lspconfig.nvim"
},



config = function()
  -- https://github.com/neovim/nvim-lspconfig
  require("mason").setup()
  require("mason-lspconfig").setup({
    ensure_installed = { "lua_ls", "ltex", "pyright", "texlab", "bashls", 'marksman', 'julials' }
  })

  local lspconfig = require('lspconfig')
  lspconfig.pyright.setup {}
  lspconfig.texlab.setup {}
  lspconfig.bashls.setup{}
  lspconfig.r_language_server.setup{}
  lspconfig.julials.setup{}
  lspconfig.marksman.setup{
    -- also needs:
    -- $home/.config/marksman/config.toml :
    -- [core]
    -- markdown.file_extensions = ["md", "markdown", "qmd"]
    filetypes = { "markdown", "quarto" },
    root_dir = require("lspconfig.util").root_pattern(".git", ".marksman.toml", "_quarto.yml"),
  }

  lspconfig.lua_ls.setup {
    on_init = function(client)
      local path = client.workspace_folders[1].name
      if not vim.loop.fs_stat(path..'/.luarc.json') and not vim.loop.fs_stat(path..'/.luarc.jsonc') then
        client.config.settings = vim.tbl_deep_extend('force', client.config.settings, {
          Lua = {
            runtime = {
              -- Tell the language server which version of Lua you're using
              -- (most likely LuaJIT in the case of Neovim)
              version = 'LuaJIT'
            },
            -- Make the server aware of Neovim runtime files
            workspace = {
              checkThirdParty = false,
              library = {
                vim.env.VIMRUNTIME
                -- "${3rd}/luv/library"
                -- "${3rd}/busted/library",
              }
              -- or pull in all of 'runtimepath'. NOTE: this is a lot slower
              -- library = vim.api.nvim_get_runtime_file("", true)
            }
          }
        })

        client.notify("workspace/didChangeConfiguration", { settings = client.config.settings })
      end
      return true
    end
  }


  -- Global mappings.
  -- See `:help vim.diagnostic.*` for documentation on any of the below functions
  vim.keymap.set('n', '<space>e', vim.diagnostic.open_float, {desc = "open diagnostic"})
  vim.keymap.set('n', '[d', vim.diagnostic.goto_prev, {desc = "go to prev diagnostic"})
  vim.keymap.set('n', ']d', vim.diagnostic.goto_next, {desc = "go to next diagnostic"})
  vim.keymap.set('n', '<space>q', vim.diagnostic.setloclist, {desc = "list diagnostics"})

  -- Use LspAttach autocommand to only map the following keys
  -- after the language server attaches to the current buffer
  vim.api.nvim_create_autocmd('LspAttach', {
    group = vim.api.nvim_create_augroup('UserLspConfig', {}),
    callback = function(ev)
      -- Enable completion triggered by <c-x><c-o>
      vim.bo[ev.buf].omnifunc = 'v:lua.vim.lsp.omnifunc'

      -- Buffer local mappings.
      -- See `:help vim.lsp.*` for documentation on any of the below functions
      -- will uncomment the keymaps below as I need them
      -- vim.keymap.set('n', 'gD', vim.lsp.buf.declaration, { buffer = ev.buf })
      -- vim.keymap.set('n', 'gd', vim.lsp.buf.definition, { buffer = ev.buf })
      -- vim.keymap.set('n', 'K', vim.lsp.buf.hover, { buffer = ev.buf })
      -- vim.keymap.set('n', 'gi', vim.lsp.buf.implementation, { buffer = ev.buf })
      -- vim.keymap.set('n', '<C-k>', vim.lsp.buf.signature_help, { buffer = ev.buf })
      -- vim.keymap.set('n', '<space>wa', vim.lsp.buf.add_workspace_folder, { buffer = ev.buf })
      -- vim.keymap.set('n', '<space>wr', vim.lsp.buf.remove_workspace_folder, { buffer = ev.buf })
      -- vim.keymap.set('n', '<space>wl', function()
      --   print(vim.inspect(vim.lsp.buf.list_workspace_folders()))
      -- end, { buffer = ev.buf })
      -- vim.keymap.set('n', '<space>D', vim.lsp.buf.type_definition, { buffer = ev.buf })
      -- vim.keymap.set('n', '<space>rn', vim.lsp.buf.rename, { buffer = ev.buf })
      vim.keymap.set({ 'n', 'v' }, '<space>ca', vim.lsp.buf.code_action, { buffer = ev.buf, desc = "code actions" })
      -- vim.keymap.set('n', 'gr', vim.lsp.buf.references, { buffer = ev.buf })
      -- vim.keymap.set('n', '<space>f', function()
      --   vim.lsp.buf.format { async = true }
      -- end, { buffer = ev.buf })
    end,
  })
end
}

