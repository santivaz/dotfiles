vim.keymap.set('n', '<leader>h', ':nohlsearch<CR>', {desc = "clear search"}) -- clear previous search
vim.keymap.set('n', '<leader>tw', ':set wrap! <CR>', {desc = "toggle wrap"}) -- toggle wrap setting
