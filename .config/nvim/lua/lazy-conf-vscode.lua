-- plugins go here
local plugins = {

  -- LSP --------------------------------
  -- { import = "plugins.lsp-config" },

  -- COMPLETION -------------------------
  -- { import = "plugins.cmp" },

  -- SYNTAX -----------------------------
  { import = "plugins.treesitter" },

  -- VISUAL -----------------------------
  'ap/vim-css-color',                 -- preview colours in source code
  -- { import = "plugins.lualine" },
  -- { import = "plugins.gruvbox" },
  -- { import = "plugins.indent-blankline" },

  -- SNIPPETS ---------------------------
  -- { import = "plugins.luasnip" },

  -- UTILITIES --------------------------
  { import = "plugins.auto-save" },
  -- { import = "plugins.telescope" },
  -- { import = "plugins.tree" },
  { import = "plugins.todo-comments" },
  -- { import = "plugins.autopairs" },
  -- { import = "plugins.toggleterm" },
  -- { import = "plugins.guess-indent" },
  -- { import = "plugins.which-key" },
  { import = "plugins.leap" },

  -- JUPYTER ----------------------------
  -- { import = "plugins.jupyter-setup" },

  -- JULIA ------------------------------
  -- 'JuliaEditorSupport/julia-vim',

  -- LATEX ------------------------------
  -- { import = "plugins.vimtex" },

  -- FIX: markview deprecated a bunch of settings. Needs fixing.
  -- { import = 'plugins.markview' },
  { import = 'plugins.neorg' },

}

-- Bootstrap lazy.nvim
local lazypath = vim.fn.stdpath("data") .. "/lazy/lazy.nvim"
if not (vim.uv or vim.loop).fs_stat(lazypath) then
  local lazyrepo = "https://github.com/folke/lazy.nvim.git"
  local out = vim.fn.system({ "git", "clone", "--filter=blob:none", "--branch=stable", lazyrepo, lazypath })
  if vim.v.shell_error ~= 0 then
    vim.api.nvim_echo({
      { "Failed to clone lazy.nvim:\n", "ErrorMsg" },
      { out, "WarningMsg" },
      { "\nPress any key to exit..." },
    }, true, {})
    vim.fn.getchar()
    os.exit(1)
  end
end
vim.opt.rtp:prepend(lazypath)

local opts = {
  root = vim.fn.stdpath("data") .. "/lazy-vscode", -- directory where plugins will be installed
  -- Configure any other settings here. See the documentation for more details.
  -- colorscheme that will be used when installing plugins.
  -- install = { colorscheme = { "habamax" } },
  -- automatically check for plugin updates
  checker = { enabled = true },
}
require("lazy").setup(plugins, opts)
