--[[
          ███╗   ██╗███████╗ ██████╗ ██╗   ██╗██╗███╗   ███╗
          ████╗  ██║██╔════╝██╔═══██╗██║   ██║██║████╗ ████║
          ██╔██╗ ██║█████╗  ██║   ██║██║   ██║██║██╔████╔██║
          ██║╚██╗██║██╔══╝  ██║   ██║╚██╗ ██╔╝██║██║╚██╔╝██║
          ██║ ╚████║███████╗╚██████╔╝ ╚████╔╝ ██║██║ ╚═╝ ██║
          ╚═╝  ╚═══╝╚══════╝ ╚═════╝   ╚═══╝  ╚═╝╚═╝     ╚═╝ 
--]]


-- nvim-tree strongly advised to include this
vim.g.loaded_netrw = 1
vim.g.loaded_netrwPlugin = 1

vim.g.maplocalleader = ','
vim.g.mapleader = ' '

require("options")
require("keymaps")
if vim.g.vscode then
  -- VSCode extension
  -- vim.opt.packpath = vim.fn.stdpath("data") .. "/lazy-vscode"
  require("lazy-conf-vscode")
else
  -- ordinary Neovim
  -- vim.opt.packpath = vim.fn.stdpath("data") .. "/lazy"
  require("lazy-conf")
end
-- vim.lsp.set_log_level("debug")
