#
# .zshrc
#


# XDG user directories
# I don't know if it is necessary but I think things brake if this is not at the top
export XDG_DATA_HOME="$HOME/.local/share"
export XDG_CONFIG_HOME="$HOME/.config"
export XDG_STATE_HOME="$HOME/.local/state"
export XDG_CACHE_HOME="$HOME/.cache"


#### ZSH ####
setopt extendedglob nomatch notify
unsetopt beep
bindkey -v

# Set the zsh dotfiles location
export ZDOTDIR=$HOME/.config/zsh
export HISTFILE="$XDG_STATE_HOME"/zsh/history

# history
SAVEHIST=1000  # Save most-recent 1000 lines

# Completion files: Use XDG dirs
autoload -Uz compinit
zstyle ':completion:*' cache-path "$XDG_CACHE_HOME"/zsh/zcompcache
zstyle ':completion:*' rehash true
compinit -d "$XDG_CACHE_HOME"/zsh/zcompdump-$ZSH_VERSION

# prompts
# TODO: make it so that there is an indication that I have entered another shell, e.g. $ poetry shell
autoload -Uz promptinit
promptinit
if [[ $(id -u) -eq 0 ]]; then
  # red color for the root user
  color1="red"
else
  # transparent for the normal user
  color1="grey"
fi
# select a random color for for the prompt every time
color_list=(blue green yellow magenta red)
color2=$color_list[$((1 + $RANDOM % $#color_list))]
# Notice that the color needs to be repeated. This is because for long paths, the
# prompt wraps around and the color of the path becomes the third one rather than 
# the second one.
prompt adam1 $color1 $color2 $color2



#### ALIASES ####
# colors
alias ls='ls --color=auto'
alias grep='grep --color=auto'

# security
alias doas='doas -- '
alias sudo='doas'
alias cp='cp -i'

# navigation
export LSGNT=$HOME/documents/lsgnt
alias lsgnt='cd $LSGNT/year-2'

# maintenance
alias cleanup='doas pacman -Rns $(pacman -Qtdq)'
alias getmirrors='doas reflector -f 30 -l 30 --number 10 --verbose --save /etc/pacman.d/mirrorlist'

# editing
alias vim='nvim'
alias dotfiles='nvim /home/santi/.dotfiles/' # changed from dot because of conflicts with graphviz

# TheFuck for correcting commands
eval "$(thefuck --alias f)"

# force wayland
alias discord='discord --enable-features=UseOzonePlatform --ozone-platform=wayland'

# programs
alias photoprism='cd /home/santi/.photoprism && doas docker compose up -d'
function yy() { # wrapper for Yazi that changes current directory when exiting.
	local tmp="$(mktemp -t "yazi-cwd.XXXXXX")"
	yazi "$@" --cwd-file="$tmp"
	if cwd="$(cat -- "$tmp")" && [ -n "$cwd" ] && [ "$cwd" != "$PWD" ]; then
		cd -- "$cwd"
	fi
	rm -f -- "$tmp"
}

## Python related aliases

# Activate poetry virtual environment. This avoids creating the shell which breaks image.nvim
alias poetry-activate='source $(poetry env info --path)/bin/activate'

# Activate virtual env at./venv
alias venv-activate='source .venv/bin/activate'

# empty jupyter notebook
# It's not very nice to have this here but whatever
alias empty_notebook='echo "{
 \"cells\": [],
 \"metadata\": {
  \"kernelspec\": {
   \"display_name\": \"Python 3 (ipykernel)\",
   \"language\": \"python\",
   \"name\": \"python3\"
  },
  \"language_info\": {
   \"codemirror_mode\": {
    \"name\": \"ipython\",
    \"version\": 3
   },
   \"file_extension\": \".py\",
   \"mimetype\": \"text/x-python\",
   \"name\": \"python\",
   \"nbconvert_exporter\": \"python\",
   \"pygments_lexer\": \"ipython3\",
   \"version\": \"3.10.12\"
  }
 },
 \"nbformat\": 4,
 \"nbformat_minor\": 4
}"'


#### SSH-AGENT ####
# Now starting the ssh-agent as a systemd user unit
# The reason is that keepassxc was not finding the agent when launching Hyprland with SDDM
# keepassxc needs teh SSH_AUTH_SOCK variable manually overridden to the value below
export SSH_AUTH_SOCK="$XDG_RUNTIME_DIR/ssh-agent.socket"



#### ENV VARIABLES ####
export GIT_EDITOR=nvim
export EDITOR=nvim

export TASKDDATA=/var/lib/taskd

# https://wiki.archlinux.org/title/XDG_Base_Directory
export ANDROID_HOME="$XDG_DATA_HOME"/android # .android/adbkeys will still be created by adb
export CARGO_HOME="${XDG_DATA_HOME}/cargo"
export DOT_SAGE="${XDG_CONFIG_HOME}/sage"
export GNUPGHOME="${XDG_DATA_HOME}/gnupg"
export GOPATH="${XDG_DATA_HOME}/go"
export GRADLE_USER_HOME="${XDG_DATA_HOME}/gradle"
export IPYTHONDIR="${XDG_CONFIG_HOME}/ipython"
export JULIA_DEPOT_PATH="$XDG_DATA_HOME/julia:$JULIA_DEPOT_PATH"
export JUPYTER_CONFIG_DIR="${XDG_CONFIG_HOME}/jupyter"
export LESSHISTFILE="${XDG_CACHE_HOME}/less/history"
export NPM_CONFIG_USERCONFIG="$XDG_CONFIG_HOME"/npm/npmrc # Requires additional actions. See xdg-ninja
export PYTHONSTARTUP="${XDG_CONFIG_HOME}/python/pythonrc" # Requires additional actions. See xdg-ninja
export TEXMFHOME="${XDG_DATA_HOME}/texmf" # path for tex packages etc
export TEXMFVAR="$XDG_CACHE_HOME"/texlive/texmf-var
export WINEPREFIX="$XDG_DATA_HOME"/wine # Requires additional action.
export XINITRC="${XDG_CONFIG_HOME}/X11/xinitrc"
export _JAVA_OPTIONS=-Djava.util.prefs.userRoot="${XDG_CONFIG_HOME}/java" # Some apps still ignore this
# export XAUTHORITY="$XDG_RUNTIME_DIR"/Xauthority # CAREFUL Might not work as intended. Not compatible with LightDM, SLiM and SDDM
alias svn="svn --config-dir $XDG_CONFIG_HOME/subversion"
alias wget=wget --hsts-file="$XDG_DATA_HOME/wget-hsts" # I suspect MultiMc still creates this file
# alias monerod=monerod --data-dir "$XDG_DATA_HOME"/bitmonero

# misbehaving programs go here
rm -rf $HOME/.zoom/ #fuck zoom


#### ARCHIVE EXTRACTION ####
# usage: ex <file>
ex ()
{
  if [ -f "$1" ] ; then
    case $1 in
      *.tar.bz2)   tar xjf $1   ;;
      *.tar.gz)    tar xzf $1   ;;
      *.bz2)       bunzip2 $1   ;;
      *.rar)       unrar x $1   ;;
      *.gz)        gunzip $1    ;;
      *.tar)       tar xf $1    ;;
      *.tbz2)      tar xjf $1   ;;
      *.tgz)       tar xzf $1   ;;
      *.zip)       unzip $1     ;;
      *.Z)         uncompress $1;;
      *.7z)        7z x $1      ;;
      *.deb)       ar x $1      ;;
      *.tar.xz)    tar xf $1    ;;
      *.tar.zst)   unzstd $1    ;;
      *)           echo "'$1' cannot be extracted via ex()" ;;
    esac
  else
    echo "'$1' is not a valid file"
  fi
}

### OTHER ###
# eval "$(zoxide init zsh)" # weird cd command thats supposed to be smarter. But it slows down startup so not gonna use it for now.


#### PATH ####
PATH="${PATH}:$HOME/scripts:$HOME/.local/bin"
export PATH
