#!/bin/sh

# TODO: add some option to just toggle the configuration

laptop_position='left' # default laptop position
while getopts ":rl" option; do
  case $option in
    r) # Set laptop position
      laptop_position='right'
      ;;
    l) # Set laptop position
      laptop_position='left'
      ;;
    \?) # Invalid option
      echo "Error: Invalid option"
      exit;;
  esac
done

monitors=$(hyprctl monitors all | grep Monitor | awk '{print $2;}')

if [ $laptop_position == 'right' ]; then
  rightmonitor=$(echo $monitors | awk '{print $1;}')
  leftmonitor=$(echo $monitors | awk '{print $2;}')
else
  rightmonitor=$(echo $monitors | awk '{print $2;}')
  leftmonitor=$(echo $monitors | awk '{print $1;}')
fi


if [ -n "$(echo $monitors | awk '{print $2;}')" ]; then
  hyprctl keyword monitor ${leftmonitor},preferred,auto-left,1
  hyprctl keyword monitor ${rightmonitor},preferred,auto-right,1
  notify-send --transient --urgency=low --app-name="hyprland" --replace-id=73635 "Monitor setup" "Right monitor: ${rightmonitor}
Left monitor: ${leftmonitor}"
else
  hyprctl keyword monitor eDP-1,preferred,auto,1 # auto for the monitor name here does not seem to work for some reason
  # notify-send --transient --urgency=low --app-name="hyprland" --replace-id=73635 "Monitor setup" "Single monitor"
fi

